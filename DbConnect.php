<?php

class DbConnect {
    public $pdo;
    
    public function __construct() {
        $settings = $this->getPDOSettings();
        $this->pdo = new \PDO(
            $settings['dsn'],
            $settings['user'],
            $settings['pass'],
            [
                \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
                \PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"
            ]
        );
    }
    
    protected function getPDOSettings() {
        $config = include_once 'db_config.php';
        $result['dsn'] = "{$config['type']}:host={$config['host']};dbname={$config['dbname']};charset={$config['charset']};";
        $result['user'] = $config['user'];
        $result['pass'] = $config['pass'];
        return $result;
    }
    
    public function execute($query, array $params=null) {
        if (is_null($params)) {
            $stmt = $this->pdo->query($query);
            return $stmt->fetchAll();
        }
    
        $stmt = $this->pdo->prepare($query);
        $stmt->execute($params);
        return $stmt;
    }
}