<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <title>Ввод и просмотр заданий</title>
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">
</head>
<body>
<div class="container h1 mt-3">Ввод и просмотр заданий</div>
<header class="container mt-3 d-flex flex-nowrap">
    <nav class="col-md-6">
        <span>Сортировка:</span>
        <a id="sun" href="index.php?sortby=user_name">По имени</a>
        <a id="se" href="index.php?sortby=email">По емэйл</a>
        <a id="ss" href="index.php?sortby=status">По статусу</a>
    </nav>
    <nav class="col-md-6 d-flex flex-nowrap">
        <span>Страницы: </span>
        <ul class="d-flex flex-wrap list-unstyled">
            <?php for ($y = 1; $y <= $pages['pages']; $y++) { ?>
                <li class="flex-fill pl-2"><a id="sun" href="index.php?page=<?= $y ?>"><?= $y ?></a></li>
            <?php } ?>
        </ul>
    </nav>
</header>
<div class="main container">
    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped table-hover">
                <thead class="thead-dark"> <!-- table header -->
                <tr class="row">
                    <th class="col-md-2">Имя</th>
                    <th class="col-md-2">Емэйл</th>
                    <th class="col-md-7">Задание</th>
                    <th class="col-md-1">Статус</th>
                    <?php if (isset($_SESSION['login']) && $_SESSION['login'] == true) { ?>
                        <th class="col-md-1">Редакт</th>
                    <?php } ?>
                </tr>
                </thead>
                <form action="" method="post">
                    <tbody class="body-light"> <!-- table body -->
                    <?php foreach ($params as $param) { ?>
                        <tr class="row">
                            <td class="col-md-1"><?= $param['user_name'] ?></td>
                            <td class="col-md-2"><?= $param['email'] ?></td>
                            <td class="col-md-7"><?= $param['task'] ?></td>
                            <?php
                            if (isset($_SESSION['login']) && $_SESSION['login'] == true) {
                                $check_id = 'cb' . $param['id'];
                                $checked = $param['status'] == 1 ? ' checked disabled ' : ' onclick="checkB(this);" ';
                                $status = '<input type="checkbox" name="status" id="' . $check_id . '" data-iden="' . $param['id'] . '"
                                        ' . $checked . ' >';
                                $style = '';
                            } else {
                                $status = $param['status'] == 0 ? 'O' : "V";
                                $style = $param['status'] == 0 ? 'text-danger' : "text-success";
                            }
                            ?>
                            <td class="col-md-1 font-weight-bold <?= $style ?>"><?= $status ?></td>
                            <?php if (isset($_SESSION['login']) && $_SESSION['login'] == true) { ?>
                                <td class="col-md-1">
                                    <div class="btn btn-secondary" data-id="<?= $param['id'] ?>"
                                         onclick="editTask(this);">Edit
                                    </div>
                                </td>
                            <?php } ?>
                        </tr>
                    <?php } ?>
                    </tbody>
                </form>
            </table>
        </div>
    </div>
</div>

<div class="form container">
    <form action="index.php" method="post">
        <input type="text" name="user_name" required placeholder="Введите имя">
        <input type="hidden" name="mode" value="create">
        <input type="email" name="email" required placeholder="Введите емэйл"><br>
        <textarea name="task" cols="40" required placeholder="Введите задачу"></textarea><br>
        <input type="submit">
    </form>
    <hr>
</div>

<div class="container mt-3 mb-5">
    <div class="btn" style="cursor: pointer;" onclick="showform(this);">Войти</div>
    <div id="admin" class="d-none">
        <form action="" method="post">
            <input type="text" name="login" id="login" required placeholder="Login">
            <input type="password" name="passwd" required placeholder="Password">
            <input type="submit">
        </form>
    </div>
</div>

<script>
  function showform (elem) {
    var adm = document.getElementById('admin')
    adm.classList.remove('d-none')
    elem.style.cursor = 'none'
    var lgn = document.getElementById('login')
    lgn.focus()
  }

  function checkB (elem) {
    var id = elem.dataset.iden
    var xhttp = new XMLHttpRequest()
    xhttp.open('GET', 'http://beejeetest.alter-eco.info/?id=' + id + '&mode=check', true)
    xhttp.onreadystatechange = function () {//Call a function when the state changes.
      if (xhttp.readyState == 4 && xhttp.status == 200) {
        elem.disabled = true
      }
    }
    xhttp.send()
  }

  function editTask (elem) {
    var id = elem.dataset.id
    console.log('elem: ' + elem)
    console.log('id: ' + id)
    location.href = 'http://beejeetest.alter-eco.info?id=' + id + '&mode=edit'
  }

</script>
</body>
</html>
