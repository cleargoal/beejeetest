<?php

namespace Controllers;
use App\Controller;

class Home extends Controller {
    public function index ()
    {
        return $this->render('Home');
    }
}