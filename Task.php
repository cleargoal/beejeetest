<?php

include_once 'DbConnect.php';

class Task {
    protected $model, $db, $table;

    public function __construct () {
        $this->db = new DbConnect();
        $this->table = 'todo_list';
    }

    public function create($user_name, $email, $task, $status=0) {
        $this->model = [
            'user_name' => $user_name,
            'email' => $email,
            'task' => $task,
            'status' => $status,
        ];
        $query = 'insert into ' . $this->table . ' (user_name, email, task, status) values(:user_name, :email, :task, :status)';
        return $this->db->execute($query, $this->model);
    }

    public function getOne($id) {
        $query = 'select * from ' .$this->table. ' where id='.$id;
        return $this->db->execute($query)->fetch();
    }

    public function getAll($on_page = 0, $per_page = 3, $sort_by = null) {
        $query = 'select * from ' .$this->table;
        if(empty($sort_by)) {
            $query .= ' limit ' .$on_page. ', ' . $per_page;
        }
        else {
            $query .= ' order by '.$sort_by. ' limit ' .$on_page. ', ' . $per_page;
        }

        $x = $this->db->execute($query);

        return $x;
    }

    public function setStatus($id, $field, $value) {
        $query = 'update '.$this->table.' set '.$field.' = '.$value.' where id = '.$id;
        return $this->db->execute($query);
    }

    public function update($user_name, $email, $task, $status, $id) {
        $query =
            'update ' . $this->table .
            ' set user_name="' . $user_name .
            '",  email="' . $email .
            '",  task="' . $task .
            '",  status="' . $status .
            '" where id=' . $id;
        return $this->db->execute($query);
    }

    public function count() {
        $query = 'select count(*) from ' .$this->table;
        return intval($this->db->execute($query)[0][0]);
    }

}
