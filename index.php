<?php
session_start(['cookie_lifetime' => 300,]);
mb_internal_encoding("UTF-8");
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

/** setup */
$_data = !empty($_POST) ? $_POST : $_GET;

if(isset($_data['login']) && isset($_data['passwd'])) {
    $_SESSION['login'] = true;
}
/** session var for pages */
if(isset($_data['page'])) {
    $_SESSION['page'] = $_data['page'];
}
elseif(!isset($_data['page']) && isset($_SESSION['page'])) {
    $_data['page'] = $_SESSION['page'];
}
/** session var for sorting */
if(isset($_data['sortby'])) {
    $_SESSION['sortby'] = $_data['sortby'];
}
elseif(!isset($_data['sortby']) && isset($_SESSION['sortby'])) {
    $_data['sortby'] = $_SESSION['sortby'];
}

include_once 'Controller.php';
$controller = new Controller($_data );

/** save edited Task */
if(isset($_data['mode'])) {
    if($_data['mode'] == 'update') {
        $controller->update();
    }
    elseif($_data['mode'] == 'create'){
        $controller->create();
    }
    elseif($_data['mode'] == 'edit') {
        $controller->editTask($_data['id']);
    }
    elseif($_data['mode'] == 'check') {
        $controller->setStatus();
    }
}
else {
    $controller->getList();
}