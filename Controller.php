<?php
mb_internal_encoding("UTF-8");
include_once 'Task.php';

class Controller {

    protected $task_model, $input, $config;
    public function __construct ($_data) {
        $this->task_model = new Task();
        $this->input = $_data;
        $this->config = include_once 'app_config.php';
        $this->input['per_page'] = $this->config['per_page'];
    }
    
    public function getList() {
        $this->input['page'] = !empty($this->input['page']) ? intval($this->input['page']) : 1;
        $next_page = ($this->input['page']-1) * $this->input['per_page'];
        $this->input['sortby'] = !empty($this->input['sortby']) ? $this->input['sortby'] : '';
        $list = $this->task_model->getAll($next_page, $this->input['per_page'], $this->input['sortby']);
        
        $records = $this->task_model->count();
        $pages['pages'] = ceil($records/$this->input['per_page']);
        $pages['per_page'] = $this->input['per_page'];
        $pages['last_page'] = $records - $pages['per_page'];
        
        $this->render('task_view', $list, $pages);
    }
    
    public function render ($viewName, array $params, array $pages = [])
    {
        $viewFile = $viewName . '.php';
        include_once $viewFile;
    }
    
    public function editTask($id) {
        $task = $this->task_model->getOne($id);
        $this->render('editor', $task);
    }
    
    public function update() {
        $input = $this->input;
        $input['status'] = isset($input['status']) ? $input['status'] : 0;
        $this->task_model->update(
            $input["user_name"],
            $input["email"],
            $input["task"],
            $input["status"],
            $input["id"]
        );
        $this->getList();
    }
    
    public function setStatus(){
        $this->task_model->setStatus($this->input['id'], 'status', 1);
    }
    
    public function create(){
        $this->task_model->create($this->input['user_name'], $this->input['email'], $this->input['task']);
        $this->getList();
    }
    
}