<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <title>Изменение задания</title>
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">
</head>
<body>
<div class="container h1 mt-3">Изменение задания</div>
<div class="main container">
    <div class="row">
        <div class="col-md-8 offset-md-2">
            <div class="form container">
                <form action="" method="post">
                    <?foreach ($params as $param) {?>
                    <input type="text" name="user_name" value="<?=$param['user_name']?>">
                    <input type="hidden" name="id" value="<?=$param['id']?>">
                    <input type="email" name="email" value="<?=$param['email']?>"><br>
                    <textarea name="task" cols="40" ><?=$param['task']?></textarea><br>
                    <label for="status">Выполнена?</label>
                    <input type="checkbox" id="status" <?=$param['status'] == 1 ? 'checked' : ''; ?> >
                    <?}?>
                    <input type="hidden" name="mode" value="update">
                    <input type="submit">
                </form>
            </div>
        </div>
    </div>
</div>
</body>
</html>
